<?php

declare(strict_types=1);

namespace AutoAction\Event;

/**
 * Interface EventInterface
 *
 * @package AutoAction\Event;
 * @date    17/10/2018
 *
 * @author  Lucas Trindade <lucas.silva@autoavaliar.com.br>
 */
interface EventInterface
{
    /**
     * Executa a ação do evento
     */
    public function handle();
}