<?php

declare(strict_types=1);

namespace AutoAction\Event;

/**
 * Class Dispatcher
 *
 * @package AutoAction\Event;
 * @date    17/10/2018
 *
 * @author  Lucas Trindade <lucas.silva@autoavaliar.com.br>
 */
final class Dispatcher
{
    /**
     * Armazena a instância da classe
     *
     * @var Dispatcher $instance
     */
    private static $instance;

    /**
     * Singleton
     * Devolve a instância da classe atual. Caso não exista, cria uma nova
     *
     * @return Dispatcher
     */
    public static function getInstance(): Dispatcher
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Despacha o evento informado
     *
     * @param EventInterface $event
     */
    public function dispatch(EventInterface $event)
    {
        $event->handle();
    }
}